package app6;

public class Smartphone {
	/**
	 * classe representant un smartphone
	 */
	private String marque;
	private String modele;
	private String numeroTelephone;
	/**
	 * constructeur du smartphone
	 * @param umarque
	 * 	 contient la marque du smartphone
	 * @param umodele
	 *  contient le modele
	 * @param unumero
	 * contient le numero
	 */
	
	public Smartphone(String umarque, String umodele, String unumero)
	{
		marque=umarque;
		modele=umodele;
		numeroTelephone=unumero;
		
	}
	/**
	 * retourne la marque du smartphone
	 * @return
	 */
	public String getMarque()
	{
		return marque;
	}
	/**
	 * retourne le modele du smartphone
	 * @return
	 */
	public String getModele()
	{
		return modele;
	}
	/**
	 * retourne le numero detelephone
	 * @return
	 */
	public String getNumeroTelephone()
	{
		return numeroTelephone;
	}
	/**
	 * change la valeur du numero de telephone
	 * @param lenumero
	 */
	public void setNumero( String lenumero)
	{
		numeroTelephone=lenumero;
	}
	/**
	 * affiche les donn�es du smartphone
	 */
	public void afficher()
	{
		System.out.println("la marque du smatphone est:"+marque);
		System.out.println("le modele du smatphone est:"+modele);
		System.out.println("le numero du smatphone est:"+numeroTelephone);
	}
	

}
