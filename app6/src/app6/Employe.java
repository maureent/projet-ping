package app6;
import java.util.ArrayList;
public class Employe {
	/**
	 * classe representant un employe
	 */
	private String nom;
	private String prenom;
	private int annee;
	private ArrayList<Smartphone> liste=new ArrayList<Smartphone>();
	/**
	 * 
	 * @param lenom 
	 * le nom de l'employe
	 * @param leprenom
	 * le prenom de l'employe
	 * @param lannee
	 * l'annee de naissance de l'employe
	 */
	public Employe(String lenom, String leprenom, int lannee)
	{
		nom=lenom;
		prenom=leprenom;
		annee=lannee;
	}
	/**
	 * 
	 * @return
	 * retourne le nom de l'employ�
	 */
	public String getNom()
	{
		return nom;
	}
	/**
	 * retourne le prenom de l'employ�
	 * @return
	 */
	public String getPrenom()
	{
		return prenom;
	}
	/**
	 * retourne l'ann�e de naissance de l'employ�
	 * @return
	 */
	public int getAnnee()
	{
		return annee;
	}
	/**
	 * modifie le nom de l'employ�
	 * @param lenom
	 */
	public void setNom( String lenom)
	{
		nom=lenom;
	}
	/**
	 * modifie l'ann�e de naissance de l'employ�
	 * @param lannee
	 */
	public void setAnnee( int lannee)
	{
		annee=lannee;
	}
	/**
	 * calcule l'�ge de l'employ�
	 * @param anneecourante
	 * @return
	 */
	public int calculerage( int anneecourante)
	{
		return (anneecourante-annee);
		
	}
	/**
	 * ajoute un smartphone � la liste de smartphone de l'employ�
	 * @param smart
	 */
	public void ajouterSmartphone( Smartphone smart)
	{
		liste.add(smart);
	}
	/**
	 * supprime un smartphone de la liste de smartphone de l'employ�
	 * @param smart
	 */
	public void supprimerSmartphone( Smartphone smart)
	{
		liste.remove(smart);
	}
	/**
	 * affiche le nombre de smartphone d'une marque donn�e
	 * @param marque
	 * @return
	 */
	public int rechercherMarque(String marque)
	{
		int nombre=0;
		for(int i=0; i<liste.size(); i++)
		{
			if(liste.get(i).getMarque()==marque)
				nombre++;
		}
		return nombre;
			
	}
	/**
	 * affiche toutes les donn�es d'un employ�, y compris celles de son smartphonea
	 */
	public void afficher()
	{
		System.out.println("nom:"+nom);
		System.out.println("prenom:"+prenom);
		System.out.println("annee de naissance:"+annee);
		for(int i=0; i<liste.size(); i++)
		{
			System.out.println("smartphone numero:"+(i+1));
			liste.get(i).afficher();
		}
		
	}
	
}
