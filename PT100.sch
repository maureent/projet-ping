*version 9.1 56610983
u 581
SUM? 2
MULT? 2
GAIN? 2
V? 6
H? 2
I? 2
U? 6
R? 12
C? 3
? 4
@libraries
@analysis
.TRAN 1 0 0 0
+0 0ns
+1 10s
+3 .1m
.PROBE 0 0 1 1 0 3
@targets
@attributes
@translators
a 0 u 13 0 0 0 hln 100 PCBOARDS=PCB
a 0 u 13 0 0 0 hln 100 PSPICE=PSPICE
a 0 u 13 0 0 0 hln 100 XILINX=XILINX
@setup
unconnectedPins 0
connectViaLabel 0
connectViaLocalLabels 0
NoStim4ExtIFPortsWarnings 1
AutoGenStim4ExtIFPorts 1
@index
pageloc 1 0 10676 
@status
n 0 118:00:09:12:31:47;1515497507 e 
s 0 118:00:09:12:32:21;1515497541 e 
c 118:00:09:12:31:28;1515497488
*page 1 0 970 720 iA
@ports
port 182 agnd 230 160 h
port 184 +5v 230 120 h
a 1 x 3 0 0 0 hcn 100 LABEL=+Vcc
port 185 +5v 290 120 h
a 1 x 3 0 0 0 hcn 100 LABEL=-Vcc
port 183 agnd 290 160 h
port 11 AGND 325 205 h
port 188 agnd 550 50 u
port 186 +5v 610 130 h
a 1 x 3 0 0 0 hcn 100 LABEL=+Vcc
port 351 +5v 790 150 h
a 1 x 3 0 0 0 hcn 100 LABEL=+Vcc
port 187 +5v 610 190 u
a 1 x 3 0 0 0 hcn 100 LABEL=-Vcc
port 352 +5v 790 210 u
a 1 x 3 0 0 0 hcn 100 LABEL=-Vcc
port 371 AGND 685 255 h
port 437 +5v 705 505 v
a 1 x 3 0 0 0 hcn 100 LABEL=+Vcc
port 440 AGND 835 505 h
port 443 +5v 770 505 h
a 1 x 3 0 0 0 hcn 100 LABEL=Vdec
port 12 AGND 355 245 h
port 20 AGND 420 150 h
port 42 AGND 470 65 h
port 558 +5v 425 290 v
a 1 x 3 0 0 0 hcn 100 LABEL=Vdec
port 579 +5v 465 340 u
a 1 x 3 0 0 0 hcn 100 LABEL=-Vcc
port 580 +5v 465 280 h
a 1 x 3 0 0 0 hcn 100 LABEL=+Vcc
@parts
part 180 vdc 230 120 h
a 1 u 13 0 -11 18 hcn 100 DC=15V
a 0 sp 0 0 22 37 hln 100 PART=vdc
a 0 a 0:13 0 0 0 hln 100 PKGREF=V4
a 1 ap 9 0 24 7 hcn 100 REFDES=V4
part 181 vdc 290 120 h
a 0 sp 0 0 22 37 hln 100 PART=vdc
a 0 a 0:13 0 0 0 hln 100 PKGREF=V5
a 1 ap 9 0 24 7 hcn 100 REFDES=V5
a 1 u 13 0 -11 18 hcn 100 DC=-15V
part 4 GAIN 330 155 h
a 0 sp 0 0 0 30 hln 100 PART=GAIN
a 0 a 0:13 0 0 0 hln 100 PKGREF=GAIN1
a 1 ap 0 0 0 0 hln 100 REFDES=GAIN1
a 0 u 13 0 16 22 hln 100 GAIN=.4
part 357 r 695 255 h
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 x 0:13 0 0 0 hln 100 PKGREF=R8
a 0 xp 9 0 15 0 hln 100 REFDES=R8
part 358 r 740 255 h
a 0 u 13 0 15 25 hln 100 VALUE=2.2k
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 x 0:13 0 0 0 hln 100 PKGREF=R7
a 0 xp 9 0 15 0 hln 100 REFDES=R7
part 158 r 590 250 h
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 x 0:13 0 0 0 hln 100 PKGREF=R2
a 0 xp 9 0 15 0 hln 100 REFDES=R2
part 70 VSIN 325 170 h
a 0 a 0:13 0 0 0 hln 100 PKGREF=V3
a 1 ap 9 0 20 10 hcn 100 REFDES=V3
a 1 u 0 0 0 0 hcn 100 DC=0
a 1 u 0 0 0 0 hcn 100 AC=0
a 1 u 0 0 0 0 hcn 100 VOFF=25
a 1 u 0 0 0 0 hcn 100 VAMPL=25
a 1 u 0 0 0 0 hcn 100 FREQ=1
part 141 c 585 100 v
a 0 x 0:13 0 0 0 hln 100 PKGREF=C2
a 0 xp 9 0 25 35 hln 100 REFDES=C2
a 0 sp 0 0 0 10 hlb 100 PART=c
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=CK05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 u 13 0 15 35 hln 100 VALUE=68u
part 128 lm324 570 140 h
a 0 sp 11 0 14 70 hcn 100 PART=lm324
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=DIP14
a 0 s 0:13 0 0 0 hln 100 GATE=A
a 0 a 0:13 0 0 0 hln 100 PKGREF=U1
a 0 ap 9 0 56 8 hcn 100 REFDES=U1A
part 6 VDC 355 205 h
a 1 ap 9 0 9 -8 hcn 100 REFDES=V2
a 1 u 13 0 29 24 hcn 100 DC=100V
a 0 sp 0 0 22 37 hln 100 PART=VDC
a 0 a 0:13 0 0 0 hln 100 PKGREF=V2
part 2 SUM 370 205 h
a 0 sp 0 0 16 32 hln 100 PART=SUM
a 0 a 0:13 0 0 0 hln 100 PKGREF=SUM1
a 1 ap 0 0 10 2 hln 100 REFDES=SUM1
part 3 MULT 400 205 h
a 0 sp 0 0 16 32 hln 100 PART=MULT
a 0 a 0:13 0 0 0 hln 100 PKGREF=MULT1
a 1 ap 0 0 10 2 hln 100 REFDES=MULT1
part 35 IDC 460 85 h
a 0 a 0:13 0 0 0 hln 100 PKGREF=I1
a 1 ap 9 0 20 10 hcn 100 REFDES=I1
a 0 sp 11 0 24 24 hln 100 PART=IDC
a 1 u 13 0 27 35 hcn 100 DC=1mA
part 19 H 460 140 H
a 0 s 11 0 10 34 hln 100 PART=H
a 0 a 0:13 0 0 0 hln 100 PKGREF=H1
a 1 ap 9 0 10 4 hln 100 REFDES=H1
part 135 r 520 140 h
a 0 x 0:13 0 0 0 hln 100 PKGREF=R3
a 0 xp 9 0 15 0 hln 100 REFDES=R3
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 u 13 0 15 25 hln 100 VALUE=1k
part 140 r 550 100 v
a 0 x 0:13 0 0 0 hln 100 PKGREF=R4
a 0 xp 9 0 25 0 hln 100 REFDES=R4
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 u 13 0 15 0 hln 100 VALUE=1k
part 436 r 795 505 h
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 x 0:13 0 0 0 hln 100 PKGREF=R14
a 0 xp 9 0 15 0 hln 100 REFDES=R14
a 0 u 13 0 15 25 hln 100 VALUE=470k
part 435 r 715 505 h
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 x 0:13 0 0 0 hln 100 PKGREF=R15
a 0 xp 9 0 15 0 hln 100 REFDES=R15
a 0 u 13 0 15 25 hln 100 VALUE=180k
part 348 lm324 750 160 h
a 0 sp 11 0 14 70 hcn 100 PART=lm324
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=DIP14
a 0 s 0:13 0 0 0 hln 100 GATE=A
a 0 a 0:13 0 0 0 hln 100 PKGREF=U2
a 0 ap 9 0 56 8 hcn 100 REFDES=U2A
part 455 r 800 255 h
a 0 u 13 0 15 25 hln 100 VALUE=6.8k
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 x 0:13 0 0 0 hln 100 PKGREF=R12
a 0 xp 9 0 15 0 hln 100 REFDES=R12
part 157 r 505 250 h
a 0 x 0:13 0 0 0 hln 100 PKGREF=R5
a 0 xp 9 0 15 0 hln 100 REFDES=R5
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 u 13 0 15 25 hln 100 VALUE=1k
part 557 lm324 425 290 h
a 0 sp 11 0 14 70 hcn 100 PART=lm324
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=DIP14
a 0 s 0:13 0 0 0 hln 100 GATE=A
a 0 a 0:13 0 0 0 hln 100 PKGREF=U5
a 0 ap 9 0 56 8 hcn 100 REFDES=U5A
part 1 titleblk 970 720 h
a 1 s 13 0 350 10 hcn 100 PAGESIZE=A
a 1 s 13 0 180 60 hcn 100 PAGETITLE=
a 1 s 13 0 340 95 hrn 100 PAGECOUNT=1
a 1 s 13 0 300 95 hrn 100 PAGENO=1
part 461 nodeMarker 325 155 h
a 0 s 0 0 0 0 hln 100 PROBEVAR=
a 0 a 0 0 4 22 hlb 100 LABEL=1
part 468 nodeMarker 665 160 h
a 0 s 0 0 0 0 hln 100 PROBEVAR=
a 0 a 0 0 4 22 hlb 100 LABEL=3
part 462 nodeMarker 865 185 h
a 0 s 0 0 0 0 hln 100 PROBEVAR=
a 0 a 0 0 4 22 hlb 100 LABEL=2
@conn
w 195
a 0 up 0:33 0 0 0 hln 100 V=
s 325 210 325 205 191
a 0 up 33 0 327 207 hlt 100 V=
w 282
a 0 up 0:33 0 0 0 hln 100 V=
s 550 50 585 50 276
a 0 up 33 0 567 49 hct 100 V=
s 550 60 550 50 151
s 585 50 585 70 155
w 373
a 0 up 0:33 0 0 0 hln 100 V=
s 685 255 695 255 372
a 0 up 33 0 690 254 hct 100 V=
w 354
a 0 up 0:33 0 0 0 hln 100 V=
s 750 200 740 200 353
s 740 200 740 255 359
a 0 up 33 0 742 227 hlt 100 V=
s 740 255 735 255 361
w 457
a 0 up 0:33 0 0 0 hln 100 V=
s 780 255 800 255 456
a 0 up 33 0 790 254 hct 100 V=
w 8
a 0 up 0:33 0 0 0 hln 100 V=
a 0 sr 0 0 0 0 hln 100 LABEL=Temp_en_�C
s 325 155 330 155 73
a 0 up 33 0 335 154 hct 100 V=
a 0 sr 3 0 326 153 hcn 100 LABEL=Temp_en_�C
s 325 155 325 170 205
a 0 sr 3 0 327 162 hln 100 LABEL=Temp_en_�C
w 474
s 715 505 705 505 470
w 439
a 0 up 0:33 0 0 0 hln 100 V=
s 755 505 770 505 438
a 0 up 33 0 782 504 hct 100 V=
s 770 505 795 505 483
w 174
a 0 up 0:33 0 0 0 hln 100 V=
a 0 sr 0 0 0 0 hln 100 LABEL=lo
s 650 160 665 160 168
a 0 sr 3 0 657 158 hcn 100 LABEL=lo
s 665 250 630 250 172
s 665 160 665 250 170
a 0 up 33 0 667 205 hlt 100 V=
w 286
a 0 up 0:33 0 0 0 hln 100 V=
s 550 110 565 110 277
a 0 up 33 0 567 109 hct 100 V=
s 585 110 585 100 149
s 550 100 550 110 142
s 570 140 565 140 273
s 565 110 585 110 489
s 565 110 565 140 148
s 565 140 560 140 490
w 160
a 0 up 0:33 0 0 0 hln 100 V=
s 550 250 545 250 332
s 550 250 550 180 175
a 0 up 33 0 552 215 hlt 100 V=
s 570 180 550 180 159
s 590 250 550 250 327
w 16
a 0 up 0:33 0 0 0 hln 100 V=
s 380 195 380 155 209
a 0 up 33 0 360 152 hct 100 V=
w 499
a 0 up 0:33 0 0 0 hln 100 V=
s 370 205 355 205 213
a 0 up 33 0 368 204 hct 100 V=
w 57
a 0 up 0:33 0 0 0 hln 100 V=
s 410 140 410 195 58
a 0 up 33 0 412 167 hlt 100 V=
s 420 140 410 140 56
w 53
a 0 up 0:33 0 0 0 hln 100 V=
s 460 150 460 205 68
s 460 205 430 205 64
a 0 up 33 0 445 204 hct 100 V=
w 44
a 0 up 0:33 0 0 0 hln 100 V=
s 460 65 460 85 115
s 470 65 460 65 113
a 0 up 33 0 469 64 hct 100 V=
w 95
a 0 up 0:33 0 0 0 hln 100 V=
s 460 140 460 125 99
a 0 up 33 0 462 132 hlt 100 V=
s 460 140 490 140 523
s 490 140 490 25 525
s 490 25 730 25 527
s 730 25 730 160 529
s 730 160 750 160 531
w 466
a 0 up 0:33 0 0 0 hln 100 V=
a 0 sr 0:3 0 847 178 hcn 100 LABEL=out
s 865 180 830 180 369
a 0 sr 3 0 847 178 hcn 100 LABEL=out
a 0 up 33 0 847 179 hct 100 V=
s 510 220 510 140 543
s 510 140 520 140 545
s 395 220 510 220 541
s 395 390 395 220 539
s 865 180 905 180 533
s 865 185 865 180 467
s 865 255 865 185 460
s 840 255 865 255 458
s 905 180 905 390 535
s 905 390 395 390 537
w 570
a 0 up 0:33 0 0 0 hln 100 V=
s 425 330 410 330 571
s 410 330 410 365 573
s 410 365 515 365 575
s 505 250 510 250 324
a 0 up 33 0 507 249 hct 100 V=
s 505 250 495 250 559
s 495 250 495 290 561
s 495 290 515 290 563
s 515 290 515 310 565
s 515 310 505 310 567
s 515 365 515 310 577
@junction
j 230 160
+ s 182
+ p 180 -
j 230 120
+ s 184
+ p 180 +
j 290 120
+ s 185
+ p 181 +
j 290 160
+ s 183
+ p 181 -
j 325 205
+ s 11
+ w 195
j 330 155
+ p 4 IN
+ w 8
j 380 155
+ p 4 OUT
+ w 16
j 550 50
+ s 188
+ w 282
j 590 250
+ p 158 1
+ w 160
j 550 250
+ w 160
+ w 160
j 630 250
+ p 158 2
+ w 174
j 735 255
+ p 357 2
+ w 354
j 695 255
+ p 357 1
+ w 373
j 685 255
+ s 371
+ w 373
j 790 150
+ s 351
+ p 348 V+
j 790 210
+ s 352
+ p 348 V-
j 750 200
+ p 348 -
+ w 354
j 550 60
+ p 140 2
+ w 282
j 550 100
+ p 140 1
+ w 286
j 800 255
+ p 455 1
+ w 457
j 740 255
+ p 358 1
+ w 354
j 780 255
+ p 358 2
+ w 457
j 325 155
+ p 461 pin1
+ w 8
j 585 70
+ p 141 2
+ w 282
j 585 100
+ p 141 1
+ w 286
j 665 160
+ p 468 pin1
+ w 174
j 570 180
+ p 128 -
+ w 160
j 610 130
+ s 186
+ p 128 V+
j 650 160
+ p 128 OUT
+ w 174
j 610 190
+ s 187
+ p 128 V-
j 325 210
+ p 70 -
+ w 195
j 325 170
+ p 70 +
+ w 8
j 715 505
+ p 435 1
+ w 474
j 705 505
+ s 437
+ w 474
j 835 505
+ s 440
+ p 436 2
j 755 505
+ p 435 2
+ w 439
j 795 505
+ p 436 1
+ w 439
j 770 505
+ s 443
+ w 439
j 570 140
+ p 128 +
+ w 286
j 565 110
+ w 286
+ w 286
j 560 140
+ p 135 2
+ w 286
j 565 140
+ w 286
+ w 286
j 545 250
+ p 157 2
+ w 160
j 380 195
+ p 2 IN1
+ w 16
j 370 205
+ p 2 IN2
+ w 499
j 355 245
+ p 6 -
+ s 12
j 355 205
+ p 6 +
+ w 499
j 400 205
+ p 3 IN2
+ p 2 OUT
j 410 195
+ p 3 IN1
+ w 57
j 420 150
+ p 19 4
+ s 20
j 460 150
+ p 19 2
+ w 53
j 430 205
+ p 3 OUT
+ w 53
j 420 140
+ p 19 3
+ w 57
j 460 140
+ p 19 1
+ w 95
j 460 125
+ p 35 -
+ w 95
j 460 85
+ p 35 +
+ w 44
j 470 65
+ s 42
+ w 44
j 750 160
+ p 348 +
+ w 95
j 865 180
+ w 466
+ w 466
j 520 140
+ p 135 1
+ w 466
j 830 180
+ p 348 OUT
+ w 466
j 840 255
+ p 455 2
+ w 466
j 865 185
+ p 462 pin1
+ w 466
j 425 290
+ s 558
+ p 557 +
j 425 330
+ p 557 -
+ w 570
j 505 250
+ p 157 1
+ w 570
j 505 310
+ p 557 OUT
+ w 570
j 515 310
+ w 570
+ w 570
j 465 340
+ s 579
+ p 557 V-
j 465 280
+ s 580
+ p 557 V+
@attributes
a 0 s 0:13 0 0 0 hln 100 PAGETITLE=
a 0 s 0:13 0 0 0 hln 100 PAGENO=1
a 0 s 0:13 0 0 0 hln 100 PAGESIZE=A
a 0 s 0:13 0 0 0 hln 100 PAGECOUNT=1
@graphics
t 111 t 6 240 230 435 265 0 77 d_info:,,,,,,,OFF,,,,,,, 
La Pt100 est mod�lis�e par :
R(T)=100+ 0.4*T
avec R(T) en Ohms et T en �C

t 85 t 5 165 26 405 50 0 37 d_info:,,,,,,,,,,,,,14, 
Mod�lisation de la Pt100 lin�aris�e

