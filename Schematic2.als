* Schematics Aliases *

.ALIASES
V_V1            V1(+=0 -=Ve )
V_V2            V2(+=Vp -=0 )
E_MULT1          MULT1(OUT=DSB IN1=Vp IN2=Ve )
R_R2            R2(1=BLU 2=0 )
E_LOPASS1          LOPASS1(OUT=BLU IN=DSB )
_    _(Ve=Ve)
_    _(Vp=Vp)
_    _(DSB=DSB)
_    _(BLU=BLU)
.ENDALIASES

