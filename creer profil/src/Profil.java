import java.util.ArrayList;

public class Profil {
	private String libelle;
	private ArrayList<Personne> listePersonne;
	

		public Profil(String libelle) {
			this.libelle=libelle;
			listePersonne=new ArrayList<Personne>();
			
		}
		
		 /**
	     * Retourne la libell� du profil
	     * @return le libell� du profil
	     */
	    public String getLibelle() {
		return libelle;
	    }

	    /**
	     * Affecter un libelle au profil
	     * @param libelle le libell� du profil
	     */
	    public void setLibelle(String libelle) {
		this.libelle = libelle;
	    }
		
		public void ajouterPersonne(Personne personne) {
			listePersonne.add(personne);
		}
}
