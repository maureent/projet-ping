import java.util.Date;
import java.text.DateFormat;

public class Personne {
	private String nom;
	private String prenom;
	private String dateNaissance;
	private String fonction;
	
	 /**
     * Constructeur paramétré pour intialiser les attributs d'une personne
     * @param nom nom de la personne
     * @param prenom prénom de la personne
     * @param dateNaissance date de naissane de la personne
     * @param fonction fonction de la personne
     */
	
	public Personne(String nom, String prenom, String dateNaissance, String fonction ){
		this.nom=nom;
		this.prenom=prenom;
		this.dateNaissance=dateNaissance;
		this.fonction=fonction;
	}
	
	 /**
     * Retourne le nom de la personne
     * @return le nom de la personne
     */
    public String getNom() {
	return nom;
    }

    /**
     * Affecter un nom à la personne
     * @param nom le nom à attribuer à la personne
     */
    public void setNom(String nom) {
	this.nom = nom;
    }

    /**
     * Retourne le prénom de la personne
     * @return le prénom de la personne
     */
    public String getPrenom() {
	return prenom;
    }

    /**
     * Affecter un prénom à la personne
     * @param prenom le prénom de la personne 
     */
    public void setPrenom(String prenom) {
	this.prenom = prenom;
    }
    
    /**
     * Retourne la date de naissance de la personne
     * @return la date de naissance de la personne
     */
    
    public String getDateNaissance() {
    	return dateNaissance;
        }
    
    /**
     * Affecter une date de naissance à la personne
     * @param dateNaissance la date de naissance de la personne 
     */
    
    public void setDateNaissance(String dateNaissance) {
    	this.dateNaissance=dateNaissance;
        }
    
    /**
     * Retourne la fonction de la personne
     * @return la fonction de la personne
     */
    public String getFonction() {
	return dateNaissance;
    }
    
    /**
     * Affecter une fonction à la personne
     * @param fonction la fonction à attribuer à la personne
     */
    public void setFonction(String fonction) {
	this.fonction=fonction;
    }
}
