
//import de la classe Scanner
import java.util.Scanner; 
//import de la classe JOptionPane
import javax.swing.JOptionPane; 

/**
 * Programme principal avec la m�thode main
 * @author D�partement TIC - ESIGELEC
 * @version 2.1
 */
public class SimulateurAppX {

	public static void main(String[] args) {
		// cr�ation de l'environnement et r�cup�ration du terrain
		Terrain t = Environnement.creerEnvironnement(10, 10);

		// creation du robot
		Robot robot = new Robot(0, 0, "sud");

		// ajout du robot sur le terrain
		t.ajouterRobot(robot);
        
		//ajout des deux salles
		t.ajouterSalles();
		
		//ajout des quatres salles (utile pour la variante 2)
		//t.ajouterQuatreSalles();
		
		//ajout victime dans l'une des deux salles
		t.ajouterVictimeSalle();
		
		//ajout victime dans l'une des quatre salles (utile pour la variante 2)
		//t.ajouterVictimeQuatreSalles();
		// met � jour les composants graphiques
		t.updateIHM();
		
		//ajouter ici le code de d�placement du robot
		for (int i=0;i<5;i++)
		{
			robot.avancer();
		}
		System.out.println(robot.getdistanceparcourue());
		System.out.println(robot.getdistancerestante());
	}
	Robotconnectee rb=new Robotconnectee(2,3,"est");
	t.ajouterRobot(rb);
	rb.avancer();
	rb.avancer();
	rb.afficher();

}
