import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class AmiGui extends JFrame{
	 private JPanel  container;
	 private JLabel  jlNom    = new JLabel();
	    private JLabel  jlPrenom = new JLabel();
	    private JLabel  jlpied  = new JLabel();
	    private JButton jBtnAfficher;
	    
	    public AmiGui()
	    {
	    	this.setTitle("un ami");
	    	this.setSize(500, 500);
	    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    	this.setLocationRelativeTo(null);
	    	container = new JPanel();
	    	container.setLayout(new FlowLayout());
	    	jBtnAfficher = new JButton();
	    	jBtnAfficher.setText("Afficher");

	    	container.add(jlNom);
	    	container.add(jlPrenom);
	    	container.add(jlpied);

	    	this.setContentPane(container);
	    	this.setVisible(true);
	    	
	    }
	    public JLabel getJlNom() {
	    	return jlNom;
	        }
	    public JLabel getJlprenom() {
	    	return jlPrenom;
	        }
	    public JLabel getJlpied() {
	    	return jlpied;
	        }

}
