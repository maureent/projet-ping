
public class Ami {

	
		private String nom;
		private String prenom;
		private int pied;
		
		public Ami( String nom, String prenom, int pied)
		{
			this.nom=nom;
			this.prenom=prenom;
			this.pied=pied;
		}
		public String getnom()
		{
			return nom;
		}
		
		public String getprenom()
		{
			return prenom;
		}
		public int getpied()
		{
			return pied;
		}
		public void setnom(String nom)
		{
			this.nom=nom;
		}
		public void setprenom(String prenom)
		{
			this.prenom=prenom;
		}
		public void setpied(int pied)
		{
			this.pied=pied;
		}
		public void afficher() {
			System.out.println("nom:"+nom+"prenom:"+prenom+"pied:"+pied);
		    }

}
