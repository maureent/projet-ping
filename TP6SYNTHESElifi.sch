*version 9.1 1799523794
u 152
R? 7
C? 9
U? 6
V? 6
? 3
@libraries
@analysis
.AC 1 3 0
+0 101
+1 100
+2 100K
@targets
@attributes
@translators
a 0 u 13 0 0 0 hln 100 PCBOARDS=PCB
a 0 u 13 0 0 0 hln 100 PSPICE=PSPICE
a 0 u 13 0 0 0 hln 100 XILINX=XILINX
@setup
unconnectedPins 0
connectViaLabel 0
connectViaLocalLabels 0
NoStim4ExtIFPortsWarnings 1
AutoGenStim4ExtIFPorts 1
@index
pageloc 1 0 7239 
@status
c 118:02:21:16:53:11;1521647591
n 2453 118:02:21:16:44:42;1521647082 e 
s 0 118:02:21:16:44:44;1521647084 e 
*page 1 0 970 720 iA
@ports
port 64 gnd_analog 260 290 h
port 101 gnd_analog 530 290 h
port 107 agnd 110 290 h
port 120 bubble 320 180 h
a 1 x 3 0 0 0 hcn 100 LABEL=-VCC
port 123 bubble 590 180 h
a 1 x 3 0 0 0 hcn 100 LABEL=-VCC
port 121 bubble 320 260 u
a 1 x 3 0 0 0 hcn 100 LABEL=+VCC
port 122 bubble 590 260 u
a 1 x 3 0 0 0 hcn 100 LABEL=+VCC
port 114 agnd 480 440 h
port 115 agnd 590 440 h
port 118 bubble 480 380 h
a 1 x 3 0 0 0 hcn 100 LABEL=-VCC
port 119 bubble 590 380 h
a 1 x 3 0 0 0 hcn 100 LABEL=+VCC
@parts
part 9 C 260 280 v
a 0 u 13 0 10 45 hln 100 VALUE=0.97n
a 0 sp 0 0 0 10 hlb 100 PART=C
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=CK05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=C4
a 0 ap 9 0 15 0 hln 100 REFDES=C4
part 8 C 180 220 v
a 0 u 13 0 15 40 hln 100 VALUE=1.14n
a 0 sp 0 0 0 10 hlb 100 PART=C
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=CK05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=C3
a 0 ap 9 0 15 0 hln 100 REFDES=C3
part 100 C 530 280 v
a 0 u 13 0 15 25 hln 100 VALUE=0.4n
a 0 sp 0 0 0 10 hlb 100 PART=C
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=CK05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=C6
a 0 ap 9 0 15 0 hln 100 REFDES=C6
part 97 C 450 220 v
a 0 u 13 0 15 25 hln 100 VALUE=2.76n
a 0 sp 0 0 0 10 hlb 100 PART=C
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=CK05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=C5
a 0 ap 9 0 15 0 hln 100 REFDES=C5
part 4 r 200 240 h
a 0 u 13 0 15 25 hln 100 VALUE=10k
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=R3
a 0 ap 9 0 15 0 hln 100 REFDES=R3
part 99 r 390 240 h
a 0 u 13 0 15 25 hln 100 VALUE=10k
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=R6
a 0 ap 9 0 15 0 hln 100 REFDES=R6
part 98 r 470 240 h
a 0 u 13 0 15 25 hln 100 VALUE=10k
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=R5
a 0 ap 9 0 15 0 hln 100 REFDES=R5
part 106 vac 110 240 h
a 0 sp 0 0 0 50 hln 100 PART=vac
a 0 a 0:13 0 0 0 hln 100 PKGREF=V1
a 1 ap 9 0 20 10 hcn 100 REFDES=V1
a 0 u 13 0 -9 23 hcn 100 ACMAG=5V
part 5 r 120 240 h
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=R4
a 0 ap 9 0 15 0 hln 100 REFDES=R4
a 0 u 13 0 15 25 hln 100 VALUE=10k
part 12 lf411 280 240 U
a 0 sp 11 0 0 70 hln 100 PART=lf411
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=DIP8
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=U3
a 0 ap 9 0 14 0 hln 100 REFDES=U3
part 95 lf411 550 240 U
a 0 sp 11 0 0 70 hln 100 PART=lf411
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=DIP8
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=U5
a 0 ap 9 0 14 0 hln 100 REFDES=U5
part 140 VDC 480 390 h
a 1 u 13 0 -11 18 hcn 100 DC=-15V
a 0 sp 0 0 22 37 hln 100 PART=VDC
a 0 a 0:13 0 0 0 hln 100 PKGREF=V4
a 1 ap 9 0 24 7 hcn 100 REFDES=V4
part 141 VDC 590 390 h
a 1 u 13 0 -11 18 hcn 100 DC=15V
a 0 sp 0 0 22 37 hln 100 PART=VDC
a 0 a 0:13 0 0 0 hln 100 PKGREF=V5
a 1 ap 9 0 24 7 hcn 100 REFDES=V5
part 1 titleblk 970 720 h
a 1 s 13 0 350 10 hcn 100 PAGESIZE=A
a 1 s 13 0 180 60 hcn 100 PAGETITLE=
a 1 s 13 0 340 95 hrn 100 PAGECOUNT=1
a 1 s 13 0 300 95 hrn 100 PAGENO=1
part 113 nodeMarker 120 240 h
a 0 s 0 0 0 0 hln 100 PROBEVAR=
a 0 a 0 0 4 22 hlb 100 LABEL=2
part 112 nodeMarker 630 220 h
a 0 s 0 0 0 0 hln 100 PROBEVAR=
a 0 a 0 0 4 22 hlb 100 LABEL=1
@conn
w 40
a 0 up 0:33 0 0 0 hln 100 V=
s 260 250 260 240 39
s 260 240 280 240 41
s 260 240 240 240 43
a 0 up 33 0 250 239 hct 100 V=
w 59
a 0 up 0:33 0 0 0 hln 100 V=
s 180 220 180 240 58
s 180 240 200 240 60
s 180 240 160 240 62
a 0 up 33 0 170 239 hct 100 V=
w 66
a 0 up 0:33 0 0 0 hln 100 V=
s 260 280 260 290 65
a 0 up 33 0 262 285 hlt 100 V=
w 68
a 0 up 0:33 0 0 0 hln 100 V=
s 530 250 530 240 67
s 530 240 550 240 69
s 530 240 510 240 71
a 0 up 33 0 520 239 hct 100 V=
w 88
a 0 up 0:33 0 0 0 hln 100 V=
s 450 220 450 240 87
s 450 240 470 240 89
s 450 240 430 240 91
a 0 up 33 0 440 239 hct 100 V=
w 94
a 0 up 0:33 0 0 0 hln 100 V=
s 530 280 530 290 93
a 0 up 33 0 532 285 hlt 100 V=
w 46
a 0 up 0:33 0 0 0 hln 100 V=
s 280 200 250 200 45
s 250 200 250 170 47
s 250 170 180 170 49
s 180 170 180 190 51
s 250 170 360 170 53
a 0 up 33 0 305 169 hct 100 V=
s 360 170 360 200 55
s 360 200 360 220 57
s 360 220 390 220 102
s 390 220 390 240 104
w 109
a 0 up 0:33 0 0 0 hln 100 V=
s 110 290 110 280 108
a 0 up 33 0 112 285 hlt 100 V=
w 125
a 0 up 0:33 0 0 0 hln 100 V=
s 320 180 320 190 124
a 0 up 33 0 322 185 hlt 100 V=
w 127
a 0 up 0:33 0 0 0 hln 100 V=
s 320 250 320 260 126
a 0 up 33 0 322 255 hlt 100 V=
w 129
a 0 up 0:33 0 0 0 hln 100 V=
s 590 180 590 190 128
a 0 up 33 0 592 185 hlt 100 V=
w 131
a 0 up 0:33 0 0 0 hln 100 V=
s 590 250 590 260 130
a 0 up 33 0 592 255 hlt 100 V=
w 135
s 480 430 480 440 134
w 133
a 0 up 0:33 0 0 0 hln 100 V=
s 480 380 480 390 132
a 0 up 33 0 482 385 hlt 100 V=
w 139
s 590 430 590 440 138
w 137
a 0 up 0:33 0 0 0 hln 100 V=
s 590 380 590 390 136
a 0 up 33 0 592 385 hlt 100 V=
w 111
a 0 up 0:33 0 0 0 hln 100 V=
a 0 sr 0 0 0 0 hln 100 LABEL=IN
s 110 240 120 240 110
a 0 up 33 0 115 239 hct 100 V=
a 0 sr 3 0 115 238 hcn 100 LABEL=IN
w 74
a 0 up 0:33 0 0 0 hln 100 V=
a 0 sr 0 0 0 0 hln 100 LABEL=OUT
s 630 200 630 220 96
a 0 sr 3 0 632 210 hln 100 LABEL=OUT
s 550 200 520 200 73
s 520 200 520 170 75
s 520 170 450 170 77
s 450 170 450 190 79
s 520 170 630 170 81
a 0 up 33 0 575 169 hct 100 V=
s 630 170 630 200 85
@junction
j 510 240
+ p 98 2
+ w 68
j 470 240
+ p 98 1
+ w 88
j 530 290
+ s 101
+ w 94
j 530 240
+ w 68
+ w 68
j 450 240
+ w 88
+ w 88
j 430 240
+ p 99 2
+ w 88
j 110 290
+ s 107
+ w 109
j 120 240
+ p 113 pin1
+ p 5 1
j 320 190
+ p 12 V-
+ w 125
j 320 180
+ s 120
+ w 125
j 320 250
+ p 12 V+
+ w 127
j 320 260
+ s 121
+ w 127
j 590 180
+ s 123
+ w 129
j 590 260
+ s 122
+ w 131
j 480 380
+ s 118
+ w 133
j 480 440
+ s 114
+ w 135
j 590 380
+ s 119
+ w 137
j 590 440
+ s 115
+ w 139
j 480 430
+ p 140 -
+ w 135
j 480 390
+ p 140 +
+ w 133
j 590 430
+ p 141 -
+ w 139
j 590 390
+ p 141 +
+ w 137
j 550 240
+ p 95 +
+ w 68
j 630 220
+ p 112 pin1
+ p 95 OUT
j 590 190
+ p 95 V-
+ w 129
j 590 250
+ p 95 V+
+ w 131
j 280 200
+ p 12 -
+ w 46
j 360 220
+ p 12 OUT
+ w 46
j 360 200
+ p 12 B1
+ w 46
j 390 240
+ p 99 1
+ w 46
j 250 170
+ w 46
+ w 46
j 200 240
+ p 4 1
+ w 59
j 160 240
+ p 5 2
+ w 59
j 180 240
+ w 59
+ w 59
j 260 290
+ s 64
+ w 66
j 260 250
+ p 9 2
+ w 40
j 260 280
+ p 9 1
+ w 66
j 240 240
+ p 4 2
+ w 40
j 260 240
+ w 40
+ w 40
j 280 240
+ p 12 +
+ w 40
j 530 250
+ p 100 2
+ w 68
j 530 280
+ p 100 1
+ w 94
j 450 220
+ p 97 1
+ w 88
j 180 220
+ p 8 1
+ w 59
j 180 190
+ p 8 2
+ w 46
j 550 200
+ p 95 -
+ w 74
j 630 220
+ p 95 OUT
+ w 74
j 630 200
+ p 95 B1
+ w 74
j 450 190
+ p 97 2
+ w 74
j 520 170
+ w 74
+ w 74
j 630 220
+ p 112 pin1
+ w 74
j 110 280
+ p 106 -
+ w 109
j 120 240
+ p 5 1
+ w 111
j 110 240
+ p 106 +
+ w 111
j 120 240
+ p 113 pin1
+ w 111
@attributes
a 0 s 0:13 0 0 0 hln 100 PAGETITLE=
a 0 s 0:13 0 0 0 hln 100 PAGENO=1
a 0 s 0:13 0 0 0 hln 100 PAGESIZE=A
a 0 s 0:13 0 0 0 hln 100 PAGECOUNT=1
@graphics
