* Schematics Aliases *

.ALIASES
E_SUM1          SUM1(OUT=Vde IN1=$N_0001 IN2=Ve )
V_V1            V1(+=0 -=Ve )
V_V3            V3(+=$N_0001 -=0 )
V_V2            V2(+=0 -=$N_0002 )
R_R5            R5(1=0 2=$N_0003 )
E_MULT2          MULT2(OUT=$N_0003 IN1=AM IN2=AM )
E_MULT1          MULT1(OUT=AM IN1=$N_0002 IN2=Vde )
_    _(Vde=Vde)
_    _(Ve=Ve)
_    _(AM=AM)
.ENDALIASES

