public class TestPersonneGUI {

    public static void main(String[] args) {

	PersonneGUI pGui = new PersonneGUI();
	Personne p = new Personne("Durand", "Jean", 'h');
	pGui.getJlPrenom().setText(p.getPrenom());
	pGui.getJlNom().setText(p.getNom());
	pGui.getJlGenre().setText("(" + String.valueOf(p.getGenre()) + ")");

    }

}
