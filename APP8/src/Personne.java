public class Personne {

    
    // déclaration des attributs
    private String nom;
    private String prenom;
    private char   genre;

    /**
     * Constructeur paramétré pour intialiser les attributs d'une personne
     * @param nom nom de la personne
     * @param prenom prénom de la personne
     * @param genre genre de la personne
     */
    public Personne(String nom, String prenom, char genre) {
	this.nom = nom;
	this.prenom = prenom;
	this.genre = genre;
    }

    /**
     * Retourne le nom de la personne
     * @return le nom de la personne
     */
    public String getNom() {
	return nom;
    }

    /**
     * Affecter un nom à la personne
     * @param nom le nom à attribuer à la personne
     */
    public void setNom(String nom) {
	this.nom = nom;
    }

    /**
     * Retourne le prénom de la personne
     * @return le prénom de la personne
     */
    public String getPrenom() {
	return prenom;
    }

    /**
     * Affecter un prénom à la personne
     * @param prenom le prénom de la personne 
     */
    public void setPrenom(String prenom) {
	this.prenom = prenom;
    }

    /**
     * Retourne le genre de la personne
     * @return retourne le genre de la personne
     */
    public char getGenre() {
	return genre;
    }

    /**
     * Affecter un genre à la la personne
     * @param genre le genre de la personne
     */
    public void setGenre(char genre) {
	this.genre = genre;
    }

    /**
     * Afficher le nom, le prénom et le genre de la personne
     */
    public void afficher() {
	System.out.println(nom + " " + prenom + "(" + genre + ").");
    }
}
