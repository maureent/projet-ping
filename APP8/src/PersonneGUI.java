import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PersonneGUI extends JFrame {

    // Déclaration des attributs
    private JPanel  container;
    private JLabel  jlNom    = new JLabel();
    private JLabel  jlPrenom = new JLabel();
    private JLabel  jlGenre  = new JLabel();
    private JButton jBtnAfficher;

    /**
     * Constructeur pour créer la fenêtre
     */
    public PersonneGUI() {
	this.setTitle("Personne");
	this.setSize(600, 200);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setLocationRelativeTo(null);

	container = new JPanel();
	container.setLayout(new FlowLayout());

	jBtnAfficher = new JButton();
	jBtnAfficher.setText("Afficher");

	container.add(jlNom);
	container.add(jlPrenom);
	container.add(jlGenre);

	this.setContentPane(container);
	this.setVisible(true);
    }

    /**
     * @return Retourne le JLabel nom
     */
    public JLabel getJlNom() {
	return jlNom;
    }

    /**
     * @return Retourne le JLabel prénom
     */
    public JLabel getJlPrenom() {
	return jlPrenom;
    }

    /**
     * @return retourne le JLabel genre
     */
    public JLabel getJlGenre() {
	return jlGenre;
    }

}
