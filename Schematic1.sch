*version 9.1 2905177067
u 150
V? 4
SUM? 2
MULT? 3
R? 6
? 17
D? 3
C? 4
LOPASS? 3
@libraries
@analysis
.TRAN 1 0 0 0
+0 0ns
+1 3ms
+3 1u
@targets
@attributes
@translators
a 0 u 13 0 0 0 hln 100 PCBOARDS=PCB
a 0 u 13 0 0 0 hln 100 PSPICE=PSPICE
a 0 u 13 0 0 0 hln 100 XILINX=XILINX
@setup
unconnectedPins 0
connectViaLabel 0
connectViaLocalLabels 0
NoStim4ExtIFPortsWarnings 1
AutoGenStim4ExtIFPorts 1
@index
pageloc 1 0 3674 
@status
n 0 118:08:11:10:24:34;1536654274 e 
s 2832 118:08:11:10:24:34;1536654274 e 
c 118:08:11:10:24:30;1536654270
*page 1 0 970 720 iA
@ports
port 8 agnd 410 150 u
port 9 agnd 500 140 u
a 1 s 3 0 4 20 hln 100 LABEL=0
port 7 agnd 370 360 h
port 134 AGND 620 230 h
@parts
part 4 sum 430 270 h
a 0 sp 0 0 16 32 hln 100 PART=sum
a 0 a 0:13 0 0 0 hln 100 PKGREF=SUM1
a 1 ap 0 0 10 2 hln 100 REFDES=SUM1
part 2 vsin 370 310 u
a 0 a 0:13 0 0 0 hln 100 PKGREF=V1
a 1 ap 9 0 20 10 hcn 100 REFDES=V1
a 1 u 0 0 0 0 hcn 100 FREQ=1KHz
a 1 u 0 0 0 0 hcn 100 DC=0
a 1 u 0 0 0 0 hcn 100 AC=0
a 1 u 0 0 0 0 hcn 100 VOFF=0
a 1 u 0 0 0 0 hcn 100 VAMPL=1
part 6 vdc 410 210 u
a 0 sp 0 0 22 37 hln 100 PART=vdc
a 0 a 0:13 0 0 0 hln 100 PKGREF=V3
a 1 ap 9 0 24 7 hcn 100 REFDES=V3
a 1 u 13 0 -11 18 hcn 100 DC=2V
part 3 vsin 500 160 h
a 0 a 0:13 0 0 0 hln 100 PKGREF=V2
a 1 ap 9 0 20 10 hcn 100 REFDES=V2
a 1 u 0 0 0 0 hcn 100 DC=0
a 1 u 0 0 0 0 hcn 100 AC=0
a 1 u 0 0 0 0 hcn 100 VOFF=0
a 1 u 0 0 0 0 hcn 100 FREQ=10kHz
a 1 u 0 0 0 0 hcn 100 VAMPL=2
part 127 R 620 240 v
a 0 sp 0 0 0 10 hlb 100 PART=R
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=R5
a 0 ap 9 0 15 0 hln 100 REFDES=R5
part 97 mult 570 230 v
a 0 sp 0 0 16 32 hln 100 PART=mult
a 0 a 0:13 0 0 0 hln 100 PKGREF=MULT2
a 1 ap 0 0 10 2 hln 100 REFDES=MULT2
part 5 mult 490 270 h
a 0 sp 0 0 16 32 hln 100 PART=mult
a 0 a 0:13 0 0 0 hln 100 PKGREF=MULT1
a 1 ap 0 0 10 2 hln 100 REFDES=MULT1
part 1 titleblk 970 720 h
a 1 s 13 0 350 10 hcn 100 PAGESIZE=A
a 1 s 13 0 180 60 hcn 100 PAGETITLE=
a 1 s 13 0 340 95 hrn 100 PAGECOUNT=1
a 1 s 13 0 300 95 hrn 100 PAGENO=1
part 119 nodeMarker 580 200 h
a 0 s 0 0 0 0 hln 100 PROBEVAR=PORTEUSE
a 0 a 0 0 4 22 hlb 100 LABEL=14
part 117 nodeMarker 570 270 h
a 0 s 0 0 0 0 hln 100 PROBEVAR=
a 0 a 0 0 4 22 hlb 100 LABEL=12
@conn
w 16
a 0 up 0:33 0 0 0 hln 100 V=
s 410 210 410 240 15
s 410 240 440 240 17
a 0 up 33 0 425 239 hct 100 V=
s 440 240 440 260 19
w 22
a 0 up 0:33 0 0 0 hln 100 V=
s 410 170 410 150 21
a 0 up 33 0 412 160 hlt 100 V=
w 29
a 0 up 0:33 0 0 0 hln 100 V=
s 500 140 500 160 28
a 0 up 33 0 502 150 hlt 100 V=
w 25
a 0 sr 0 0 0 0 hln 100 LABEL=Vde
a 0 up 0:33 0 0 0 hln 100 V=
s 460 270 490 270 24
a 0 sr 3 0 475 268 hcn 100 LABEL=Vde
a 0 up 33 0 475 269 hct 100 V=
w 14
a 0 sr 0 0 0 0 hln 100 LABEL=Ve
a 0 up 0:33 0 0 0 hln 100 V=
s 370 270 430 270 13
a 0 up 33 0 410 269 hct 100 V=
a 0 sr 3 0 410 268 hcn 100 LABEL=Ve
w 54
a 0 up 0:33 0 0 0 hln 100 V=
s 370 360 370 310 55
a 0 up 33 0 372 340 hlt 100 V=
w 113
a 0 up 0:33 0 0 0 hln 100 V=
s 580 200 570 200 136
s 620 200 580 200 126
a 0 up 33 0 580 199 hct 100 V=
w 140
s 620 230 620 240 138
w 27
a 0 up 0:33 0 0 0 hln 100 V=
s 500 200 500 260 40
a 0 up 33 0 502 216 hlt 100 V=
w 147
a 0 sr 0 0 0 0 hln 100 LABEL=AM
a 0 up 0:33 0 0 0 hln 100 V=
s 520 270 570 270 98
a 0 sr 3 0 545 268 hcn 100 LABEL=AM
a 0 up 33 0 545 269 hct 100 V=
s 560 220 520 220 146
s 570 270 570 230 100
s 520 220 520 270 148
@junction
j 500 260
+ p 5 IN1
+ w 27
j 500 140
+ s 9
+ w 29
j 430 270
+ p 4 IN2
+ w 14
j 460 270
+ p 4 OUT
+ w 25
j 490 270
+ p 5 IN2
+ w 25
j 410 210
+ p 6 +
+ w 16
j 410 170
+ p 6 -
+ w 22
j 440 260
+ p 4 IN1
+ w 16
j 410 150
+ s 8
+ w 22
j 370 360
+ s 7
+ w 54
j 370 270
+ p 2 -
+ w 14
j 370 310
+ p 2 +
+ w 54
j 500 200
+ p 3 -
+ w 27
j 500 160
+ p 3 +
+ w 29
j 570 200
+ p 97 OUT
+ w 113
j 580 200
+ p 119 pin1
+ w 113
j 620 200
+ p 127 2
+ w 113
j 620 240
+ p 127 1
+ w 140
j 620 230
+ s 134
+ w 140
j 560 220
+ p 97 IN1
+ w 147
j 520 270
+ p 5 OUT
+ w 147
j 570 230
+ p 97 IN2
+ w 147
j 570 270
+ p 117 pin1
+ w 147
@attributes
a 0 s 0:13 0 0 0 hln 100 PAGETITLE=
a 0 s 0:13 0 0 0 hln 100 PAGENO=1
a 0 s 0:13 0 0 0 hln 100 PAGESIZE=A
a 0 s 0:13 0 0 0 hln 100 PAGECOUNT=1
@graphics
