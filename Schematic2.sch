*version 9.1 657613484
u 49
V? 3
MULT? 2
R? 3
? 7
LOPASS? 2
HIPASS? 2
@libraries
@analysis
.TRAN 1 0 0 0
+0 0ns
+1 2ms
+3 1u
@targets
@attributes
@translators
a 0 u 13 0 0 0 hln 100 PCBOARDS=PCB
a 0 u 13 0 0 0 hln 100 PSPICE=PSPICE
a 0 u 13 0 0 0 hln 100 XILINX=XILINX
@setup
unconnectedPins 0
connectViaLabel 0
connectViaLocalLabels 0
NoStim4ExtIFPortsWarnings 1
AutoGenStim4ExtIFPorts 1
@index
pageloc 1 0 3327 
@status
n 0 118:08:07:16:46:25;1536331585 e 
s 2832 118:08:07:16:47:51;1536331671 e 
*page 1 0 970 720 iA
@ports
port 6 AGND 280 300 h
port 8 AGND 410 130 u
port 33 AGND 750 260 h
@parts
part 2 VSIN 280 280 u
a 0 a 0:13 0 0 0 hln 100 PKGREF=V1
a 1 ap 9 0 20 10 hcn 100 REFDES=V1
a 1 u 0 0 0 0 hcn 100 AC=0
a 1 u 0 0 0 0 hcn 100 DC=
a 1 u 0 0 0 0 hcn 100 VOFF=0
a 1 u 0 0 0 0 hcn 100 FREQ=1kHz
a 1 u 0 0 0 0 hcn 100 VAMPL=2
part 3 VSIN 410 200 u
a 0 a 0:13 0 0 0 hln 100 PKGREF=V2
a 1 ap 9 0 20 10 hcn 100 REFDES=V2
a 1 u 0 0 0 0 hcn 100 AC=0
a 1 u 0 0 0 0 hcn 100 VOFF=0
a 1 u 0 0 0 0 hcn 100 VAMPL=2
a 1 u 0 0 0 0 hcn 100 FREQ=10k
part 4 MULT 400 240 h
a 0 sp 0 0 16 32 hln 100 PART=MULT
a 0 a 0:13 0 0 0 hln 100 PKGREF=MULT1
a 1 ap 0 0 10 2 hln 100 REFDES=MULT1
part 32 R 690 240 h
a 0 sp 0 0 0 10 hlb 100 PART=R
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=R2
a 0 ap 9 0 15 0 hln 100 REFDES=R2
part 31 LOPASS 560 240 h
a 0 sp 0 0 14 48 hln 100 PART=LOPASS
a 0 a 0:13 0 0 0 hln 100 PKGREF=LOPASS1
a 0 ap 0 0 -12 -2 hln 100 REFDES=LOPASS1
a 0 u 13 0 16 38 hlb 100 RIPPLE=3dB
a 0 u 13 0 42 38 hlb 100 STOP=30dB
a 0 u 13 0 52 16 hlb 100 FS=9.5Hz
a 0 u 13 0 52 28 hlb 100 FP=9Hz
part 1 titleblk 970 720 h
a 1 s 13 0 350 10 hcn 100 PAGESIZE=A
a 1 s 13 0 180 60 hcn 100 PAGETITLE=
a 1 s 13 0 300 95 hrn 100 PAGENO=1
a 1 s 13 0 340 95 hrn 100 PAGECOUNT=1
part 25 nodeMarker 310 240 h
a 0 s 0 0 0 0 hln 100 PROBEVAR=
a 0 a 0 0 4 22 hlb 100 LABEL=1
part 27 nodeMarker 410 210 h
a 0 s 0 0 0 0 hln 100 PROBEVAR=
a 0 a 0 0 4 22 hlb 100 LABEL=2
part 44 nodeMarker 660 240 h
a 0 s 0 0 0 0 hln 100 PROBEVAR=
a 0 a 0 0 4 22 hlb 100 LABEL=5
part 46 nodeMarker 520 240 h
a 0 s 0 0 0 0 hln 100 PROBEVAR=
a 0 a 0 0 4 22 hlb 100 LABEL=6
@conn
w 10
a 0 up 0:33 0 0 0 hln 100 V=
s 280 280 280 300 11
a 0 up 33 0 282 290 hlt 100 V=
w 18
a 0 up 0:33 0 0 0 hln 100 V=
s 410 130 410 160 17
a 0 up 33 0 412 145 hlt 100 V=
w 14
a 0 sr 0 0 0 0 hln 100 LABEL=Ve
a 0 up 0:33 0 0 0 hln 100 V=
s 310 240 400 240 26
a 0 sr 3 0 355 238 hcn 100 LABEL=Ve
a 0 up 33 0 355 239 hct 100 V=
s 280 240 310 240 13
w 16
a 0 sr 0 0 0 0 hln 100 LABEL=Vp
a 0 up 0:33 0 0 0 hln 100 V=
s 410 200 410 210 15
a 0 sr 3 0 412 205 hln 100 LABEL=Vp
a 0 up 33 0 412 206 hlt 100 V=
s 410 210 410 230 28
w 39
a 0 up 0:33 0 0 0 hln 100 V=
s 750 240 750 260 40
s 730 240 750 240 43
a 0 up 33 0 740 239 hct 100 V=
w 37
a 0 sr 0 0 0 0 hln 100 LABEL=BLU
a 0 up 0:33 0 0 0 hln 100 V=
s 660 240 690 240 45
a 0 sr 3 0 675 238 hcn 100 LABEL=BLU
a 0 up 33 0 675 239 hct 100 V=
s 650 240 660 240 36
w 35
a 0 sr 0 0 0 0 hln 100 LABEL=DSB
a 0 up 0:33 0 0 0 hln 100 V=
s 520 240 560 240 47
a 0 sr 3 0 540 238 hcn 100 LABEL=DSB
a 0 up 33 0 540 239 hct 100 V=
s 430 240 520 240 34
@junction
j 280 300
+ s 6
+ w 10
j 400 240
+ p 4 IN2
+ w 14
j 410 230
+ p 4 IN1
+ w 16
j 410 130
+ s 8
+ w 18
j 310 240
+ p 25 pin1
+ w 14
j 410 210
+ p 27 pin1
+ w 16
j 430 240
+ p 4 OUT
+ w 35
j 690 240
+ p 32 1
+ w 37
j 730 240
+ p 32 2
+ w 39
j 750 260
+ s 33
+ w 39
j 660 240
+ p 44 pin1
+ w 37
j 520 240
+ p 46 pin1
+ w 35
j 410 200
+ p 3 +
+ w 16
j 410 160
+ p 3 -
+ w 18
j 280 280
+ p 2 +
+ w 10
j 280 240
+ p 2 -
+ w 14
j 560 240
+ p 31 IN
+ w 35
j 650 240
+ p 31 OUT
+ w 37
@attributes
a 0 s 0:13 0 0 0 hln 100 PAGETITLE=
a 0 s 0:13 0 0 0 hln 100 PAGENO=1
a 0 s 0:13 0 0 0 hln 100 PAGESIZE=A
a 0 s 0:13 0 0 0 hln 100 PAGECOUNT=1
@graphics
